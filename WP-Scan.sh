#!/bin/bash
website="https://www.dollartreecanada.com/"
curl '$website/wp-json/wp/v2/users'
lynx -dump -nolist '$website/wp-content/uploads'
wpscan --enumerate ap --detection-mode aggressive --rua -v --url '$website' --wp-content-dir wp-content
curl 'https://api.hackertarget.com/hostsearch/?q=$website'
curl -o - -I '$website'
nmap -sC -sV '$website' && echo 'Done!'